# README

These are hobby programs for which the following hold true:

1. they're written in x86-64 assembly language;
2. using [nasm](https://www.nasm.us/);
3. for 64-bit GNU+Linux platforms;
4. using `glibc`, the GNU C Library (sometimes)


## Where to Start?

The common `"Hello, world"` program can be found [here](hello/hello.s). Programs that do *not* use `glibc` have `_start` as their main entrypoint. If linking with GCC, you must include the `-nostdlib` option. If using `glibc`, then the main entrypoint is `main` instead.


## Important Details

Assume the following basic entrypoint:

```c++
int main(int argc, char *argv[]) {
    // ...
}
```

When using the standard C library:

1. `argc` is copied to `rdi`;
2. the address of `argv` is copied to `rsi`

When *not* using the C library:

1. `argc` is copied to memory location pointed by `rsp`;
2. same as with C library

When using the C library, the *defacto* standard calling convention is `cdecl`. This has implications for register usage and other conventions.


## References

- https://www.felixcloutier.com/x86/
- https://github.com/hjl-tools/x86-psABI/wiki/X86-psABI
- https://stackoverflow.com/questions/16721164/x86-linux-assembler-get-program-parameters-from-start
- https://stackoverflow.com/questions/35864291/get-argv2-address-in-assembler-x64
- https://stackoverflow.com/questions/38335212/calling-printf-in-x86-64-using-gnu-assembler
