; This program calculates the area from two integer inputs. It's a 64-bit
; program using `glibc` with the standard C `main` function:
;
;   int main(int argc, char** argv) {
;       // ...
;   }
;
; Unlike 32-bit systems, where there're less registers and arguments
; are passed on the stack, function arguments on 64-bit systems are
; passed in registers.
;
;   rsi: The address of `argv`, pointing to CLI argument strings.
;   rdi: The value of `argc`, the number of CLI arguments in `argv`.
;
; Commands for building:
;
;   nasm -g -f elf64 -F dwarf area.s
;   gcc -O3 -no-pie -g area.o -o area
;
; Useful references:
;
;   https://stackoverflow.com/a/6212755/4594973
;   https://github.com/hjl-tools/x86-psABI/wiki/X86-psABI
;

; glibc functions
extern printf, atoi

section .rodata

    exit_success: equ 0
    exit_failure: equ 1

    usage_str: db "Usage: %s <width> <height>", 10, 0
    result_str: db "%d", 10, 0

section .bss

    ; reserve double word (32 bits); same as: int width;
    width: resd 1

section .text

global main


main:
    ; prologue: save stack frame
    push rbp
    mov rbp, rsp

    ; we use these and they're callee-saved regs
    ; so we must preserve them for the caller
    push rsi
    push rdi

    ; check that we have exactly 3 arguments
    ; counting program name at `argv[0]`
    cmp rdi, 3              ; argc == 3?
    jne .invalid_argc       ; argc != 3, so we bail out

    ; width = atoi(argv[1]);
    add rsi, 8              ; rsi = &argv[1]
    mov rdi, [rsi]          ; rdi = argv[1]
    call atoi               ; convert string to int
    mov [width], rax        ; save result: width = rax

    ; rax = height = atoi(argv[2]);
    mov rsi, [rbp-8]        ; reset back to argv
    add rsi, 16             ; rsi = &argv[2]
    mov rdi, [rsi]          ; rdi = argv[2]
    call atoi               ; rax = atoi(argv[2]), i.e. rax = height
    imul rax, [width]       ; rax *= width

    ; printf(rdi, rsi); -> printf(result_str, result)
    ; the `printf` function is variadic and must be told how
    ; many vector registers are being used; since we don't use
    ; any, `rax = 0` must be set
    mov rsi, rax            ; set final result for display
    mov rdi, result_str     ; set the result format string
    xor rax, rax            ; no vector regs used
    call printf

    mov rax, exit_success   ; set OS return code
    jmp .exit

.invalid_argc:
    ; argc != 3, so there's a problem
    mov rsi, [rsi]          ; rsi = *argv = &argv[0]
    mov rdi, usage_str      ; rdi = format string
    xor rax, rax            ; rax = 0; we're not using any vector registers
    call printf             ; show the error message
    mov rax, exit_failure   ; set OS return code

.exit:
    ; restore used callee-saved regs
    pop rdi
    pop rsi

    ; destroy stack frame
    pop rbp
    ret
