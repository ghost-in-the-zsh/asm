/**
 * Reference program for `area.s`
 */

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Usage: %s <width> <height>\n", argv[0]);
        return EXIT_FAILURE;
    }
    int w = atoi(argv[1]);
    int h = atoi(argv[2]);
    printf("%d\n", w * h);

    return EXIT_SUCCESS;
}
