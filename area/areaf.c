/**
 * Reference program for `areaf.s`
 */

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Usage: %s <width> <height>\n", argv[0]);
        return EXIT_FAILURE;
    }
    float w = atof(argv[1]);
    float h = atof(argv[2]);
    printf("%.2f\n", w * h);

    return EXIT_SUCCESS;
}
