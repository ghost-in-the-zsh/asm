; This program calculates the area from two single-precision
; floating-point inputs. This is a 64-bit program using `glibc`
; with the standard C `main` function:
;
;   int main(int argc, char** argv) {
;       // ...
;   }
;
; Unlike 32-bit systems, where there're less registers and arguments
; are passed on the stack, function arguments on 64-bit systems are
; passed in registers.
;
;   rsi: The address of `argv`, pointing to CLI argument strings.
;   rdi: The value of `argc`, the number of CLI arguments in `argv`.
;
; Commands for building:
;
;   nasm -g -f elf64 -F dwarf areaf.s
;   gcc -O3 -no-pie -g areaf.o -o areaf
;
; Useful references:
;
;   https://stackoverflow.com/a/6212755/4594973
;   https://github.com/hjl-tools/x86-psABI/wiki/X86-psABI
;   https://www.felixcloutier.com/x86/
;

; glibc functions
extern printf, atof

section .rodata

    exit_success: equ 0
    exit_failure: equ 1

    usage_str: db "Usage: %s <width> <height>", 10, 0
    result_str: db "%.2f", 10, 0

section .bss

    ; reserve double word (32 bits); same as: float width;
    width: resd 1

section .text

global main


main:
    ; prologue: save stack frame
    push rbp
    mov rbp, rsp

    ; we use these and they're callee-saved regs
    ; so we must preserve them for the caller
    push rsi
    push rdi

    ; check that we have exactly 3 arguments
    ; counting program name at `argv[0]`
    cmp rdi, 3              ; argc == 3?
    jne .invalid_argc       ; argc != 3, so we bail out

    ; xmm0 = width = atof(argv[1]);
    ; note that `atof` converts to double-precision `float`ing-point
    ; not single-precision `float`ing-point
    mov rdi, [rsi+8]        ; rdi = argv[1]
    call atof               ; xmm0 = atof(...); converts string to `double` type
    cvtsd2ss xmm0, xmm0     ; convert `double` to `float`
    movss [width], xmm0     ; move scalar single-precision

    ; xmm0 = height = atof(argv[2]);
    mov rsi, [rbp-8]        ; reset back to argv; `atof` trashes it
    mov rdi, [rsi+16]       ; rdi = argv[2]
    call atof               ; xmm0 = atof(argv[2]), i.e. xmm0 = height
    cvtsd2ss xmm0, xmm0     ; convert `double` to `float`
    mulss xmm0, [width]     ; xmm0 *= width; mult scalar single-precision
    cvtss2sd xmm0, xmm0     ; convert `float` to `double`

    ; print the result to stdout;
    ; here we're using one vector register, i.e. `xmm0`; we tell
    ; `printf` about this by setting `rax = 1` and it knows to
    ; pick up the value from `xmm0`
    mov rdi, result_str     ; set the result format string
    mov rax, 1              ; `xmm0` vector reg used
    call printf             ; result to display is already in `xmm0`

    mov rax, exit_success   ; set OS return code
    jmp .exit

.invalid_argc:
    ; argc != 3, so show the usage message
    ; and exit with an error
    mov rsi, [rsi]          ; rsi = *argv = &argv[0]
    mov rdi, usage_str      ; rdi = format string
    xor rax, rax            ; rax = 0; we're not using any vector registers
    call printf             ; show the error message
    mov rax, exit_failure   ; set OS return code

.exit:
    ; restore used callee-saved regs
    pop rdi
    pop rsi

    ; destroy stack frame
    leave                   ; does `pop rbp`
    ret
